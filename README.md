# Exchange Rate Service REST API with Spring Boot
Simple REST API for currency exchange rate service backend, implemented in
Spring Boot and Java.

## Requirements
- Docker (tested on v20.10.6)
- docker-compose (tested on v1.28.6)

## Usage
```bash
# Make sure you have Docker and docker-compose on your environment setup
# clone the repo, then build
docker-compose build

# And then run the backend with logging to stdout
docker-compose up
```

API Endpoints will be available on `http://<YOUR_TEST_MACHINE_IP>:8080/`

### Testing
Currently only on development environment (no docker)
```bash
./mvnw clean test
```

### Vagrant VM

Alternatively, deployment and testing can be performed on provided Vagrant VM
dev environment:

```bash
vagrant up          # wait some time until VM is provisioned
vagrant ssh
cd /vagrant         # the current working directory on host is mapped here
./mvnw clean test

# docker-compose is also available here
docker-compose build
docker-compose up

# the API Endpoints will be available at
# http://192.168.0.2:8080/allcurrencies
# http://192.168.0.2:8080/currencypair
# http://192.168.0.2:8080/convert
#
# IP Address is configurable in the Vagrantfile

vagrant destroy     # to remove VM
```

## Available Endpoints
### `[GET] /allcurrencies`

#### Query Parameters
- none

#### Example Result
`200 OK`
```json
{
    "timestamp": "2021-06-24",
    "base": "EUR",
    "currencies_counter": {
        "USD": 3,
        "HUF": 2,
        "SGD": 1,
        "JPY": 0,
        /* ... */
    }
}
```

### `[GET] /currencypair`

#### Query Parameters
- `from`: the source currency
- `to`: the target currency

#### Example Results
`200 OK`
```json
{
    "timestamp": "2021-06-24",
    "from": "EUR",
    "to": "USD",
    "rate": 1.19123746,
    "chart_url": "https://www.xe.com/currencycharts/?from=EUR&to=USD"
}
```

`400 Bad Request`
```json
{
    "message": "invalid currency"
}
```


### `[GET] /convert`

#### Query Parameters
- `from`: the source currency
- `to`: the target currency
- `amount`: the amount in the source currency

#### Example Results
`200 OK`
```json
{
    "timestamp": "2021-06-24",
    "from": "EUR",
    "to": "USD",
    "amount": 500,
    "value": 595.12
}
```

`400 Bad Request`
```json
{
    "message": "invalid currency"
}
```

`400 Bad Request`
```json
{
    "message": "invalid amount"
}
```

## Test Environment
### Setup
```bash
# On fresh installed Ubuntu 20.04 server
sudo apt install docker.io  # Debian upstream package is now up-to-date
sudo apt install python3-pip
sudo pip3 install docker-compose
sudo usermod -aG docker $(whoami)
newgrp docker
```

### Versions
```
$ docker --version
Docker version 20.10.6, build 370c289

$ docker-compose --version
docker-compose version 1.28.6, build unknown

$ uname -r
5.8.0-53-generic

$ cat /etc/lsb-release
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=20.04
DISTRIB_CODENAME=focal
DISTRIB_DESCRIPTION="Ubuntu 20.04.2 LTS"
```
