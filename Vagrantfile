# -*- mode: ruby -*-
# vi: set ft=ruby :
#
# For running the unit tests and general testing
#
# Usage:
# 1. install virtualbox and vagrant
# 2. vagrant up
# 3. vagrant ssh
# 4. cd /vagrant
# 5. ./mvnw clean test
#
# To run docker container
# 1. docker-compose build
# 2. docker-compose up
#
Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/focal64"

  config.vm.network "private_network", ip: "192.168.0.2"

  config.vm.provision "shell", inline: <<-SHELL
    echo "Current user: $(whoami)"
    echo "pwd: $(pwd)"
    apt-get update
    echo ""
    echo "==> Installing docker and docker-compose"
    echo ""
    apt-get install -y docker.io
    usermod -aG docker vagrant
    newgrp docker
    apt-get install -y python3-pip
    pip install docker-compose
    echo ""
    echo "==> Installing adoptopenjdk"
    echo ""
    apt-get install -y wget apt-transport-https gnupg
    wget https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public
    gpg --no-default-keyring --keyring ./adoptopenjdk-keyring.gpg --import public
    gpg --no-default-keyring --keyring ./adoptopenjdk-keyring.gpg --export --output adoptopenjdk-archive-keyring.gpg
    rm adoptopenjdk-keyring.gpg
    mv adoptopenjdk-archive-keyring.gpg /usr/share/keyrings
    echo "deb [signed-by=/usr/share/keyrings/adoptopenjdk-archive-keyring.gpg] https://adoptopenjdk.jfrog.io/adoptopenjdk/deb focal main" | sudo tee /etc/apt/sources.list.d/adoptopenjdk.list
    apt-get update
    apt-get install -y adoptopenjdk-11-hotspot
  SHELL
end
