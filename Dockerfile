# Use multi-stage build for portable compilation
FROM adoptopenjdk/openjdk11:alpine AS builder
WORKDIR /workspace/app

COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY src src

RUN ./mvnw install -DskipTests
RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)

FROM adoptopenjdk/openjdk11:alpine
VOLUME /tmp
ARG DEPENDENCY=/workspace/app/target/dependency
COPY --from=builder ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=builder ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=builder ${DEPENDENCY}/BOOT-INF/classes /app

RUN addgroup -g 1099 -S appuser && adduser -u 1099 -S appuser -G appuser

ENTRYPOINT ["java", "-cp", "app:app/lib/*", "com.exchange_rate_service.backend.BackendApplication"]
