package com.exchange_rate_service.backend;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;

/** Tests are run locally in dev environment with ./mvnw clean test */
@WebMvcTest(ExchangeServiceController.class)
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ExchangeServiceControllerTest {
  @Autowired MockMvc mockMvc;

  /** Currencies listed on ECB */
  private static final String supportedCurrencies[] = {
    "USD", "JPY", "BGN", "CZK", "DKK", "GBP", "HUF", "PLN", "RON", "SEK", "CHF", "ISK", "NOK",
    "HRK", "RUB", "TRY", "AUD", "BRL", "CAD", "CNY", "HKD", "IDR", "ILS", "INR", "KRW", "MXN",
    "MYR", "NZD", "PHP", "SGD", "THB", "ZAR", "EUR"
  };

  /**
   * Currencies not listed on ECB. Source:
   * https://en.wikipedia.org/wiki/List_of_circulating_currencies
   */
  private static final String unsupportedCurrencies[] = {
    "GGP", "XCD", "AWG", "SHP", "BSD", "BHD", "BBD", "BZD", "XOF", "BMD", "BTN", "BAM", "BND",
    "XOF", "XAF", "CVE", "KYD", "XAF", "KMF", "CKD", "ANG", "FOK", "XPF", "IMP", "JOD", "JEP",
    "KID", "KWD", "LBP", "LSL", "MOP", "TVD", "ANG", "SAR", "STN", "JOD", "PAB", "OMR", "QAR",
    "NPR", "NAD", "MAD", "MOP"
  };

  private static final String endpointAllCurrencies = "/allcurrencies";
  private static final String endpointCurrencyPairTpl = "/currencypair?from=%s&to=%s";
  private static final String endpointConvertTpl = "/convert?from=%s&to=%s&amount=%f";

  private static final String baseCurrency = "EUR";

  /**
   * Covers User Story #1
   *
   * @throws Exception
   */
  @Test
  public void getCurrencyPairWithReferenceRate_normalCases_fromEur() throws Exception {
    for (String targetCurrency : supportedCurrencies) {
      mockMvc
          .perform(
              MockMvcRequestBuilders.get(
                      String.format(endpointCurrencyPairTpl, baseCurrency, targetCurrency))
                  .contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.from", is(baseCurrency)))
          .andExpect(jsonPath("$.to", is(targetCurrency)));
    }
  }

  /**
   * Covers User Story #1
   *
   * @throws Exception
   */
  @Test
  public void getCurrencyPairWithReferenceRate_normalCases_toEur() throws Exception {
    for (String sourceCurrency : supportedCurrencies) {
      mockMvc
          .perform(
              MockMvcRequestBuilders.get(
                      String.format(endpointCurrencyPairTpl, sourceCurrency, baseCurrency))
                  .contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.to", is(baseCurrency)))
          .andExpect(jsonPath("$.from", is(sourceCurrency)));
    }
  }

  @Test
  public void getCurrencyPairWithReferenceRate_unsupportedCurrencies_shouldReturnHttp400()
      throws Exception {
    for (String sourceCurrency : unsupportedCurrencies) {
      mockMvc
          .perform(
              MockMvcRequestBuilders.get(
                      String.format(endpointCurrencyPairTpl, sourceCurrency, baseCurrency))
                  .contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isBadRequest());
    }
  }

  @Test
  public void getCurrencyPairWithReferenceRate_invalidInput() throws Exception {
    // long string, invalid characters, etc.
    // test against input sanitization
  }

  @Test
  public void getCurrencyPairWithReferenceRate_lowercaseInput() throws Exception {
    for (String sourceCurrency : supportedCurrencies) {
      for (String targetCurrency : supportedCurrencies) {
        mockMvc
            .perform(
                MockMvcRequestBuilders.get(
                        String.format(
                            endpointCurrencyPairTpl,
                            sourceCurrency.toLowerCase(Locale.ROOT),
                            targetCurrency.toLowerCase(Locale.ROOT)))
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.from", is(sourceCurrency)))
            .andExpect(jsonPath("$.to", is(targetCurrency)));
      }
    }
  }

  /**
   * Covers User Story #2
   *
   * @throws Exception
   */
  @Test
  public void getCurrencyPairNonReferenceRate_normalCases() throws Exception {
    for (String sourceCurrency : supportedCurrencies) {
      for (String targetCurrency : supportedCurrencies) {
        if (targetCurrency.equals(baseCurrency) | sourceCurrency.equals(baseCurrency)) continue;

        ResultActions resultActions =
            mockMvc
                .perform(
                    MockMvcRequestBuilders.get(
                            String.format(endpointCurrencyPairTpl, sourceCurrency, targetCurrency))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        if (sourceCurrency.equals(targetCurrency)) {
          resultActions.andExpect(jsonPath("$.rate", closeTo(1.0, 0.001)));
        }
      }
    }
  }

  @Test
  public void getCurrencyPairNonReferenceRate_unsupportedCurrencies() throws Exception {
    for (String sourceCurrency : unsupportedCurrencies) {
      for (String targetCurrency : supportedCurrencies) {
        if (targetCurrency.equals(baseCurrency)) continue;

        mockMvc
            .perform(
                MockMvcRequestBuilders.get(
                        String.format(endpointCurrencyPairTpl, sourceCurrency, targetCurrency))
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
      }
    }
  }

  @Test
  public void getCurrencyPairNonReferenceRate_invalidInput() throws Exception {}

  /**
   * Covers User Story #5
   *
   * @throws Exception
   */
  @Test
  public void getLinkToCurrencyChart() throws Exception {
    for (String sourceCurrency : supportedCurrencies) {
      for (String targetCurrency : supportedCurrencies) {
        mockMvc
            .perform(
                MockMvcRequestBuilders.get(
                        String.format(endpointCurrencyPairTpl, sourceCurrency, targetCurrency))
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.chart_url", stringContainsInOrder(sourceCurrency)))
            .andExpect(jsonPath("$.chart_url", stringContainsInOrder(targetCurrency)));
        // TODO: extend with checking if URL is valid
      }
    }
  }

  /**
   * Covers User Story #3
   *
   * @throws Exception
   */
  @Test
  public void getListOfSupportedCurrencies() throws Exception {
    int numOfValidCurrencies = supportedCurrencies.length;

    mockMvc
        .perform(
            MockMvcRequestBuilders.get(endpointAllCurrencies)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.base", is(baseCurrency)))
        .andExpect(jsonPath("$.currencies_counter.length()", is(numOfValidCurrencies)));
  }

  /**
   * Covers User Story #3
   *
   * @throws Exception
   */
  @Test
  public void getListOfSupportedCurrencies_withCounter() throws Exception {
    HashMap<String, AtomicLong> localCounter = new HashMap<>();
    int numCurrencyPairsToTest = 50;
    int supportedCurrenciesLength = supportedCurrencies.length;

    // to get deterministic and repeatable results
    Random rand = new Random(1337);

    // hit both APIs, and count them internally
    for (int i = 0; i < numCurrencyPairsToTest; i++) {
      String sourceCurrency = supportedCurrencies[rand.nextInt(supportedCurrenciesLength)];
      String targetCurrency = supportedCurrencies[rand.nextInt(supportedCurrenciesLength)];

      // hit random API endpoints
      String apiEndpoint;
      if (rand.nextBoolean()) {
        apiEndpoint = String.format(endpointCurrencyPairTpl, sourceCurrency, targetCurrency);
      } else {
        apiEndpoint = String.format(endpointConvertTpl, sourceCurrency, targetCurrency, 1.0);
      }

      mockMvc
          .perform(MockMvcRequestBuilders.get(apiEndpoint).contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk());

      // FIXME: figure out how to pass by reference and reduce copy-paste!
      if (localCounter.containsKey(sourceCurrency)) {
        localCounter.get(sourceCurrency).incrementAndGet();
      } else {
        localCounter.put(sourceCurrency, new AtomicLong(1));
        ;
      }

      if (localCounter.containsKey(targetCurrency)) {
        localCounter.get(targetCurrency).incrementAndGet();
      } else {
        localCounter.put(targetCurrency, new AtomicLong(1));
        ;
      }
    }

    // then compare the backend's state with my state
    ResultActions result =
        mockMvc
            .perform(
                MockMvcRequestBuilders.get(endpointAllCurrencies)
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());

    for (Map.Entry<String, AtomicLong> entry : localCounter.entrySet()) {
      String currency = entry.getKey();
      int count = entry.getValue().intValue();

      String currencyRegex = "$.currencies_counter." + currency;
      result.andExpect(jsonPath(currencyRegex, is(count)));
    }
  }

  @Test
  public void getValueInOtherCurrency_normalCases() throws Exception {
    for (String sourceCurrency : supportedCurrencies) {
      for (String targetCurrency : supportedCurrencies) {
        mockMvc
            .perform(
                MockMvcRequestBuilders.get(
                        String.format(endpointConvertTpl, sourceCurrency, targetCurrency, 1.0))
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.from", is(sourceCurrency)))
            .andExpect(jsonPath("$.to", is(targetCurrency)))
            .andExpect(jsonPath("$.value", notNullValue()));
        /**
         * TODO: $.value returns BigDecimal, jsonPath does not support this, had to do custom json
         * provider/parser
         */
      }
    }
  }

  @Test
  public void getValueInOtherCurrency_lowercaseInput() throws Exception {
    for (String sourceCurrency : supportedCurrencies) {
      for (String targetCurrency : supportedCurrencies) {
        mockMvc
            .perform(
                MockMvcRequestBuilders.get(
                        String.format(
                            endpointConvertTpl,
                            sourceCurrency.toLowerCase(),
                            targetCurrency.toLowerCase(),
                            1.0))
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.from", is(sourceCurrency.toUpperCase())))
            .andExpect(jsonPath("$.to", is(targetCurrency.toUpperCase())));
      }
    }
  }

  @Test
  public void getValueInOtherCurrency_unsupportedCurrencies_shouldReturnHttp400() throws Exception {
    for (String sourceCurrency : unsupportedCurrencies) {
      mockMvc
          .perform(
              MockMvcRequestBuilders.get(
                      String.format(endpointConvertTpl, sourceCurrency, baseCurrency, 1.0))
                  .contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isBadRequest());
    }
  }

  @Test
  public void getValueInOtherCurrency_missingParam() throws Exception {
    mockMvc
        .perform(
            MockMvcRequestBuilders.get("/convert?from=EUR&to=USD")
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void getValueInOtherCurrency_invalidInput() throws Exception {
    String endpointTemplate = "/convert?from=EUR&to=USD&amount=%s";

    String invalidInputs[] = {"1,0", "0,1", "-0,1", "a", "A", "-"};

    for (String invalidInput : invalidInputs) {
      mockMvc
          .perform(
              MockMvcRequestBuilders.get(String.format(endpointTemplate, invalidInput))
                  .contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isBadRequest());
    }
  }

  @Test
  public void getValueInOtherCurrency_edgeCases() throws Exception {
    /** - decimal separator - not a number - zero - negative values - max values */
  }

  private String getTodaysTimestamp() {
    // TODO: ECB Rate is updated every day at 16:00 CET, this test currently ignores it
    Timestamp ts = new Timestamp(System.currentTimeMillis());
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    String timestamp_str = simpleDateFormat.format(ts);

    return timestamp_str;
  }

  @Test
  public void checkTimestamp_endpointAllCurrencies() throws Exception {
    String timestamp_str = getTodaysTimestamp();

    mockMvc
        .perform(
            MockMvcRequestBuilders.get(endpointAllCurrencies)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.timestamp", is(timestamp_str)));
  }

  @Test
  public void checkTimestamp_endpointConvert() throws Exception {
    String timestamp_str = getTodaysTimestamp();

    for (String sourceCurrency : supportedCurrencies) {
      for (String targetCurrency : supportedCurrencies) {
        mockMvc
            .perform(
                MockMvcRequestBuilders.get(
                        String.format(endpointConvertTpl, sourceCurrency, targetCurrency, 1.0))
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.timestamp", is(timestamp_str)));
      }
    }
  }

  @Test
  public void checkTimestamp_endpointCurrencyPair() throws Exception {
    String timestamp_str = getTodaysTimestamp();

    for (String sourceCurrency : supportedCurrencies) {
      for (String targetCurrency : supportedCurrencies) {
        mockMvc
            .perform(
                MockMvcRequestBuilders.get(
                        String.format(endpointCurrencyPairTpl, sourceCurrency, targetCurrency))
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.timestamp", is(timestamp_str)));
      }
    }
  }
}
