package com.exchange_rate_service.backend;

import java.util.HashMap;
import lombok.Getter;

public class AllCurrenciesResponse {
  /** The date when the rate is retrieved. */
  @Getter private final String timestamp;

  /** The "base"/source currency, typically EUR */
  @Getter private final String base;

  /** The list of the currencies, with the hit counter */
  @Getter private final HashMap<String, Integer> currencies_counter;

  public AllCurrenciesResponse(
      String timestamp, String base, HashMap<String, Integer> currencies_counter) {
    this.timestamp = timestamp;
    this.base = base;
    this.currencies_counter = currencies_counter;
  }
}
