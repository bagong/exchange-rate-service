package com.exchange_rate_service.backend;

import lombok.Getter;

public class ConvertAmountResponse {
  /** The date when the rate is retrieved. */
  @Getter private final String timestamp;

  /** The "base"/source currency, 3-letter code, e.g. USD */
  @Getter private final String from;

  /** The target currency, 3-letter code, e.g. EUR */
  @Getter private final String to;

  /** The amount in source currency */
  @Getter private final double amount;

  /** The value in target currency */
  @Getter private final double value;

  public ConvertAmountResponse(
      String timestamp, String from, String to, double amount, double value) {
    this.timestamp = timestamp;
    this.from = from;
    this.to = to;
    this.amount = amount;
    this.value = value;
  }
}
