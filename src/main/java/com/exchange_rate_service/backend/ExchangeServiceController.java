package com.exchange_rate_service.backend;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.concurrent.atomic.AtomicLong;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.UnknownCurrencyException;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.CurrencyConversionException;
import javax.money.convert.ExchangeRate;
import javax.money.convert.ExchangeRateProvider;
import javax.money.convert.MonetaryConversions;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class ExchangeServiceController {

  private ExchangeRateProvider rateProvider;

  // TODO: pull this to application.properties
  private static final String chartUrlTemplate = "https://www.xe.com/currencycharts/?from=%s&to=%s";

  // TODO: pull this out from ECB data instead of hardcode
  private static final String supportedCurrencies[] = {
    "USD", "JPY", "BGN", "CZK", "DKK", "GBP", "HUF", "PLN", "RON", "SEK", "CHF", "ISK", "NOK",
    "HRK", "RUB", "TRY", "AUD", "BRL", "CAD", "CNY", "HKD", "IDR", "ILS", "INR", "KRW", "MXN",
    "MYR", "NZD", "PHP", "SGD", "THB", "ZAR", "EUR"
  };

  private HashMap<String, AtomicLong> currenciesHitCounter;

  public ExchangeServiceController() {
    // TODO: put local XML as fallback if no internet connection
    this.rateProvider = MonetaryConversions.getExchangeRateProvider("ECB");

    initCurrenciesHitCounter();
  }

  private void initCurrenciesHitCounter() {
    // TODO: extract currencies from ECB XML data instead of this
    currenciesHitCounter = new HashMap<>();
    for (String currency : supportedCurrencies) {
      currenciesHitCounter.putIfAbsent(currency, new AtomicLong());
    }
  }

  private static String getExchangeTimestamp() {
    // TODO: extract timestamp from the ECB's XML data, then cache it
    Timestamp ts = new Timestamp(System.currentTimeMillis());

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    String timestamp_str = simpleDateFormat.format(ts);

    return timestamp_str;
  }

  private void updateCurrenciesCounter(String... currencies) {
    for (String currency : currencies) {
      // TODO: handle error better
      assert currenciesHitCounter.containsKey(currency);
      currenciesHitCounter.get(currency).incrementAndGet();
    }
  }

  private HashMap<String, Integer> getCurrenciesHitCounter() {
    HashMap<String, Integer> currentHitCounter = new HashMap<>();

    for (String key : currenciesHitCounter.keySet()) {
      int hitCounter = currenciesHitCounter.get(key).intValue();
      currentHitCounter.put(key, hitCounter);
    }

    return currentHitCounter;
  }

  @GetMapping(value = "/allcurrencies", produces = "application/json")
  public AllCurrenciesResponse getAllCurrencies() {
    return new AllCurrenciesResponse(getExchangeTimestamp(), "EUR", getCurrenciesHitCounter());
  }

  @GetMapping(
      value = "/currencypair",
      produces = "application/json",
      params = {"from", "to"})
  public CurrencyPairResponse getCurrencyPair(@RequestParam String from, @RequestParam String to) {
    String sourceCurrency = from.toUpperCase();
    String targetCurrency = to.toUpperCase();

    ExchangeRate rate;

    try {
      rate = rateProvider.getExchangeRate(sourceCurrency, targetCurrency);
    } catch (UnknownCurrencyException e) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid currency");
    } catch (CurrencyConversionException e) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "unsupported currency");
    } catch (RuntimeException e) {
      // TODO: bare exception is generally not good
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unknown error: " + e.getMessage());
    }

    if (rate == null)
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "unsupported currency");

    updateCurrenciesCounter(sourceCurrency, targetCurrency);

    String chartUrl = String.format(chartUrlTemplate, sourceCurrency, targetCurrency);

    CurrencyPairResponse resp =
        new CurrencyPairResponse(
            getExchangeTimestamp(),
            sourceCurrency,
            targetCurrency,
            rate.getFactor().doubleValueExact(),
            chartUrl);

    return resp;
  }

  @GetMapping(
      value = "/convert",
      produces = "application/json",
      params = {"from", "to", "amount"})
  public ConvertAmountResponse convertAmount(
      @RequestParam String from, @RequestParam String to, @RequestParam double amount) {

    MonetaryAmount sourceAmount;
    MonetaryAmount targetAmount;
    String sourceCurrency = from.toUpperCase();
    String targetCurrency = to.toUpperCase();

    try {
      sourceAmount =
          Monetary.getDefaultAmountFactory().setCurrency(sourceCurrency).setNumber(amount).create();
      CurrencyConversion conversion = rateProvider.getCurrencyConversion(targetCurrency);
      targetAmount = sourceAmount.with(conversion);
    } catch (MethodArgumentTypeMismatchException | NumberFormatException e) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid amount");
    } catch (UnknownCurrencyException e) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid currency");
    } catch (RuntimeException e) {
      // TODO: bare exception is generally not good
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "unknown error: " + e.getMessage());
    }

    updateCurrenciesCounter(sourceCurrency, targetCurrency);

    ConvertAmountResponse resp =
        new ConvertAmountResponse(
            getExchangeTimestamp(),
            sourceCurrency,
            targetCurrency,
            sourceAmount.getNumber().doubleValueExact(),
            targetAmount.getNumber().doubleValueExact());
    return resp;
  }
}
