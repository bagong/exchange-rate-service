package com.exchange_rate_service.backend;

import lombok.Getter;

public class CurrencyPairResponse {
  /** The date when the rate is retrieved. */
  @Getter private final String timestamp;

  /** The "base"/source currency, 3-letter code, e.g. USD */
  @Getter private final String from;

  /** The target currency, 3-letter code, e.g. EUR */
  @Getter private final String to;

  /** The current exchange rate, in target currency */
  @Getter private final double rate;

  /** URL to online charting service between the currency pair */
  @Getter private final String chart_url;

  public CurrencyPairResponse(
      String timestamp, String from, String to, double rate, String chart_url) {
    this.timestamp = timestamp;
    this.from = from;
    this.to = to;
    this.rate = rate;
    this.chart_url = chart_url;
  }
}
